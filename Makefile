sdist: setup.py
	python setup.py sdist

upload:
	twine upload dist/*

clean:
	rm -rf dist pytorch_model_trainer.egg-info
