# Callbacks
Used to trigger events at certain points during training.  
Callbacks have access to the trainer object and thus to all its properties (history, model, ...).

# Custom callbacks
implement the Callback interface given in base_callback.py. Implement the methods needed to trigger them at the point needed.

# Example
```python
from pytorch_trainer.callbacks.base_callback import Callback

class MyCallBack(Callback):
    def on_train_begin(self):
        print("Starting Training")

    def on_train_end(self):
        print("Training is done")
        print("Here is the history")
        print(self.trainer.history)
```