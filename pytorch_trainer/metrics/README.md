# Metrics
Used to measure the performance of the model in training.  
Metrics are updated when a batch is processed by the model and added to the history after the loop is completed. The metric is then reset.

# Custom metrics
Implement the Metric interface given in base_metric.py. Implement all the abstract methods.

# Example

```python
from pytorch_trainer.metrics.base_metric import _Metric


class MyMetric(_Metric):
    def __init__(self):
        super().__init__("custom_metric")
        self.some_state = 0

    def update_state(self, y_true, y_predict):
        self.some_state += "do something with y_true and y_value"

    def result(self):
        return self.some_state

    def reset_state(self):
        self.some_state = 0
```