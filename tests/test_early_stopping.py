import unittest

from pytorch_trainer.callbacks import EarlyStopping


class MyTestCase(unittest.TestCase):

    def test_max_improvement_out_of_delta(self):
        es = EarlyStopping(min_delta=.5, mode="max")
        es.current_best_metric = 1
        result = es.comparison_method(2)

        self.assertTrue(result)

    def test_max_improvement_on_delta(self):
        es = EarlyStopping(min_delta=.5, mode="max")
        es.current_best_metric = 1
        result = es.comparison_method(1.5)

        self.assertTrue(result)

    def test_max_improvement_in_delta(self):
        es = EarlyStopping(min_delta=.5, mode="max")
        es.current_best_metric = 1
        result = es.comparison_method(1.25)

        self.assertFalse(result)

    def test_max_stagnation_out_of_delta(self):
        es = EarlyStopping(min_delta=.5, mode="max")
        es.current_best_metric = 2
        result = es.comparison_method(1)

        self.assertFalse(result)

    def test_max_stagnation_in_delta(self):
        es = EarlyStopping(min_delta=.5, mode="max")
        es.current_best_metric = 1.25
        result = es.comparison_method(1)

        self.assertFalse(result)

    def test_min_improvement_out_of_delta(self):
        es = EarlyStopping(min_delta=.5, mode="min")
        es.current_best_metric = 1
        result = es.comparison_method(.3)

        self.assertTrue(result)

    def test_min_improvement_on_delta(self):
        es = EarlyStopping(min_delta=.5, mode="min")
        es.current_best_metric = 1
        result = es.comparison_method(.5)

        self.assertTrue(result)

    def test_min_improvement_in_delta(self):
        es = EarlyStopping(min_delta=.5, mode="min")
        es.current_best_metric = 1
        result = es.comparison_method(.75)

        self.assertFalse(result)

    def test_min_stagnation_out_of_delta(self):
        es = EarlyStopping(min_delta=.5, mode="min")
        es.current_best_metric = 1
        result = es.comparison_method(1.75)

        self.assertFalse(result)

    def test_min_stagnation_in_delta(self):
        es = EarlyStopping(min_delta=.5, mode="min")
        es.current_best_metric = 1
        result = es.comparison_method(1.25)

        self.assertFalse(result)


if __name__ == '__main__':
    unittest.main()
