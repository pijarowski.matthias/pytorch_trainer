import unittest

import torch

from pytorch_trainer.metrics.classification_metrics import Precision


class MyTestCase(unittest.TestCase):

    def test_all_zero(self):
        m = Precision()
        t1 = torch.Tensor([0, 0, 0, 0])
        m.update_state(t1, t1)
        self.assertAlmostEqual(m.result(), 0)

    def test_all_one(self):
        m = Precision()
        t1 = torch.Tensor([1, 1, 1, 1])
        m.update_state(t1, t1)
        self.assertAlmostEqual(m.result(), 1)

    def test_mix(self):
        m = Precision()
        t1 = torch.Tensor([0, 1, 1, 1])
        t2 = torch.Tensor([1, 0, 1, 1])
        m.update_state(t2, t1)
        self.assertAlmostEqual(m.result(), 2/3)

    def test_logits(self):
        m = Precision(from_logit=True)
        t1 = torch.Tensor([-1, 1, .4, .6])
        t2 = torch.Tensor([0, 1, 0, 1])
        m.update_state(t2, t1)
        self.assertAlmostEqual(m.result(), 2/3)


if __name__ == '__main__':
    unittest.main()
