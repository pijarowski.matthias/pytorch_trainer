import unittest

import torch

from pytorch_trainer.metrics import Accuracy, BinaryAccuracy


class MyTestCase(unittest.TestCase):

    def test_single_updates(self):
        t1 = torch.Tensor([[0, 1, 2], [0, 2, 1]])
        t2 = torch.Tensor([2, 1])

        acc = Accuracy()
        acc.update_state(t2, t1)

        self.assertAlmostEqual(acc.result(), 1.0)

    def test_multiple_updates(self):
        acc = Accuracy()

        t1 = torch.Tensor([[0, 1, 2], [0, 2, 1]])
        t2 = torch.Tensor([2, 1])
        acc.update_state(t2, t1)

        t3 = torch.Tensor([0, 2])
        acc.update_state(t3, t1)

        self.assertAlmostEqual(acc.result(), 0.5)

    def test_with_reset(self):
        acc = Accuracy()

        t1 = torch.Tensor([[0, 1, 2], [0, 2, 1]])
        t2 = torch.Tensor([2, 1])
        acc.update_state(t2, t1)
        acc.reset_state()

        t3 = torch.Tensor([0, 2])
        acc.update_state(t3, t1)

        self.assertAlmostEqual(acc.result(), 0)

    def test_tensor_shapes_for_binary_accuracy(self):
        acc = BinaryAccuracy()

        t1 = torch.Tensor([[0.5], [0], [1]])
        t2 = torch.Tensor([0, 1, 1])
        acc.update_state(t2, t1)
        self.assertAlmostEqual(acc.result(), 1/3)

        acc.reset_state()
        t1 = torch.Tensor([0.5, 0, 1])
        acc.update_state(t2, t1)
        self.assertAlmostEqual(acc.result(), 1/3)


if __name__ == '__main__':
    unittest.main()
